from django.urls import path
from . import views


urlpatterns = [
    path('ussd', views.index, name='ussd'),
    path('', views.user_list, name='all_users'),
    path('export-users', views.export_user_csv, name='export_users'),
    path('user/<int:pk>', views.user_detail, name='user_detail'),
    path('create-user', views.create_user, name='create_user'),
    path('bulk-upload', views.user_bulk_upload, name='bulk_upload'),
    path('user-groups', views.user_groups, name='user_groups'),
    path('user-autocomplete', views.UserAutoComplete.as_view, name='user-autocomplete'),
    path('update-profiles', views.update_profiles, name='update_profile'),
]