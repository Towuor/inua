import requests
from datetime import datetime

today = datetime.today().strftime("%d/%m/%Y")


def create_customer(first_name, last_name, phone_number, email, national_id):

    payload = {
    "field_395": "No",
    "field_395_raw": "No",
    "field_133": first_name+" "+last_name,
    "field_267": "Retail - Farm Supply",
    "field_267_raw": "Retail - Farm Supply",
    "field_298": "Farm supply customer",
    "field_298_raw": "Farm supply customer",
    "field_299": "Retail",
    "field_299_raw": "Retail",
    "field_677": "Retail",
    "field_677_raw": "Retail",
    "field_135": first_name+" "+last_name,
    "field_136": phone_number,
    "field_606": phone_number,
    "field_606_raw": 254768224281,
    "field_509": national_id,
    "field_137": email,
    "field_487": email,
    "field_268": today,
    }

    try:
        my_headers = {'X-Knack-Application-Id': '6088367fcd6220001bef9a90',
                      'X-Knack-REST-API-Key': '6869438b-5ad5-45ad-add2-7fe6a279da95'}
        my_response = requests.post(
            "https://api.knack.com/v1/objects/object_15/records",
            payload,
            headers=my_headers)

        return my_response

    except Exception as e:
        print('Encountered an error while sending: %s' % str(e))


def create_farmer(first_name, last_name, phone_number, national_id):

    payload = {
    "field_395": "Yes",
    "field_395_raw": "Yes",
    "field_393": first_name + " " + last_name,
    "field_393_raw": first_name + " " + last_name,
    "field_133": first_name+" "+last_name,
    "field_267": "Retail - Farm Supply",
    "field_267_raw": "Retail - Farm Supply",
    "field_298": "Farm supply customer",
    "field_298_raw": "Farm supply customer",
    "field_299": "Retail",
    "field_299_raw": "Retail",
    "field_677": "Retail",
    "field_677_raw": "Retail",
    "field_135": first_name + " " + last_name,
    "field_135_raw": {
        "first": first_name,
        "last": last_name
    },
    "field_136": phone_number,
    "field_136_raw": phone_number,
    "field_606": phone_number,
    "field_606_raw": phone_number,
    "field_137": national_id+"@mazaobora.com",
    "field_487": national_id+"@mazaobora.com",
    "field_268": today
    }
    try:
        my_headers = {'X-Knack-Application-Id': '6088367fcd6220001bef9a90',
                      'X-Knack-REST-API-Key': '6869438b-5ad5-45ad-add2-7fe6a279da95'}
        my_response = requests.post(
            "https://api.knack.com/v1/objects/object_15/records",
            payload,
            headers=my_headers)

        return my_response

    except Exception as e:
        print('Encountered an error while sending: %s' % str(e))


def create_agrovet(agrovet_name, agrovet_county, agrovet_town, agrovet_location, email, phone_number, contact_name):
    payload = {
    "field_1": agrovet_name,
    "field_1_raw": agrovet_name,
    "field_2": agrovet_location+"<br />"+agrovet_town+", " + agrovet_county +"<br />Kenya",
    "field_2_raw": {
        "street": agrovet_location,
        "street2": "",
        "city": agrovet_town,
        "state": agrovet_county,
        "zip": "",
        "country": "Kenya",
        "latitude": 1.0151715,
        "longitude": 35.002132
    },
    "field_635": contact_name,
    "field_636": email,
    "field_637": "<a href=\"tel:"+phone_number+"\">"+phone_number+"</a>",
    "field_637_raw": {
        "formatted": phone_number,
        "number": phone_number,
        "full": phone_number
    }
    }
    try:
        my_headers = {'X-Knack-Application-Id': '6088367fcd6220001bef9a90',
                      'X-Knack-REST-API-Key': '6869438b-5ad5-45ad-add2-7fe6a279da95'}
        my_response = requests.post(
            "https://api.knack.com/v1/objects/object_1/records",
            payload,
            headers=my_headers)

        return my_response

    except Exception as e:
        print('Encountered an error while sending: %s' % str(e))


def create_supplier(supplier_name, email, phone_number, contact_name):
    payload = {
    "field_257": supplier_name,
    "field_257_raw": supplier_name,
    "field_262": today,
    "field_263": 10,
    "field_263_raw": 10,
    "field_325": "Karimi Agrovet",
    "field_325_raw": [
        {
            "id": "609a3bddf37464001d6f53c2",
            "identifier": "Karimi Agrovet "
        }
    ],


    "field_400": email,
    "field_400_raw": {
        "email": email
    },
    "field_401": "<a href=\"tel:"+phone_number+"\">"+phone_number+"</a>",
    "field_401_raw": {
        "formatted": phone_number,
        "number": phone_number,
        "full": phone_number
    },
    "field_402": contact_name
    }

    try:
        my_headers = {'X-Knack-Application-Id': '6088367fcd6220001bef9a90',
                      'X-Knack-REST-API-Key': '6869438b-5ad5-45ad-add2-7fe6a279da95'}
        my_response = requests.post(
            "https://api.knack.com/v1/objects/object_24/records",
            payload,
            headers=my_headers)

        return my_response

    except Exception as e:
        print('Encountered an error while sending: %s' % str(e))

