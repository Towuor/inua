import base64
import uuid
import datetime
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.core.mail import send_mail, EmailMessage
from django.contrib.auth.tokens import default_token_generator
from decimal import Decimal

# creating custom Django user, overriding default Django User account Setup

token_generator = default_token_generator


class CustomAccountManager(BaseUserManager):

    def create_user(self, id_number, phone_number, password):
        user = self.model(id_number=id_number, phone_number=phone_number, password=password)
        user.set_password(password)
        user.is_staff = False
        user.is_superuser = False
        user.save(using=self._db)
        return user

    def create_superuser(self, id_number, phone_number, password):
        user = self.create_user(id_number=id_number, phone_number=phone_number, password=password)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, phone_number):
        print(phone_number)
        return self.get(phone_number=phone_number)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=500, blank=True, null=True)
    last_name = models.CharField(max_length=500, blank=True, null=True)
    phone_number = models.CharField(unique=True, max_length=12)
    id_number = models.CharField(unique=True, max_length=15)
    created_at = models.DateTimeField(default=timezone.now, null=False)
    group_lead = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    REQUIRED_FIELDS = ['id_number']
    USERNAME_FIELD = "phone_number"

    objects = CustomAccountManager()

    def get_short_name(self):
        return self.phone_number

    def natural_key(self):
        return self.phone_number

    def __str__(self):
        return self.phone_number


class UserProfile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='profile', blank=True, null=True)
    card_number = models.CharField(max_length=500, blank=True, null=True)
    last_expense = models.BooleanField(default=False)
    nhif = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now, null=False)

    def __str__(self):
        return self.user.phone_number


class UserContributions(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='contributions', blank=True, null=True)
    contribution = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, null=False)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.last_name


class UserGroup(models.Model):
    name = models.CharField(max_length=1000, null=False, blank=False)
    owner = models.ForeignKey(CustomUser, related_name='team_owner', null=True, blank=True, on_delete=models.SET_NULL)
    members = models.ManyToManyField(CustomUser, related_name='team')
    contribution = models.CharField(max_length=999999, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, blank=False, null=False)
    updated_at = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender=CustomUser)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.get_or_create(user=instance)


@receiver(post_save, sender=CustomUser)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
