from dal import autocomplete
from django import forms
from .models import CustomUser, UserGroup


class UserForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        exclude = ('created_at', 'is_staff', 'is_active', 'password')
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'au-input au-input--full',
                                                 'required': 'true',
                                                 'style': 'color:black;', }),
            'last_name': forms.TextInput(attrs={'class': 'au-input au-input--full',
                                                'required': 'true'}),
            'phone_number': forms.TextInput(attrs={'class': 'au-input au-input--full',
                                                   'required': 'true'}),
            'id_number': forms.TextInput(attrs={'class': 'au-input au-input--full',
                                                'required': 'true'}),
        }


class BulkUploadForm(forms.Form):
    user_csv = forms.FileField()


class GroupForm(forms.ModelForm):
    class Meta:
        model = UserGroup
        fields = ('name', 'owner',)