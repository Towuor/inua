from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from .models import CustomUser, UserGroup, UserProfile
from .forms import UserForm, BulkUploadForm, GroupForm
from .utils import create_customer, create_farmer, create_agrovet, create_supplier
from products.models import Category, Product, Quantity
from dal import autocomplete
import africastalking
import csv
import io
import pandas as pd
import csv
import requests
import json
import knackpy
# Create your views here.

username = "inua_mkulima"
api_key = "f398a3e6c44e3fd30837a62ba12ee1c534ec703ba336541ebd55f4b3fc937dee"
africastalking.initialize(username, api_key)
sms = africastalking.SMS
sender_id = 'KARIMI-LTD'
app = knackpy.App(app_id="6088367fcd6220001bef9a90",  api_key="6869438b-5ad5-45ad-add2-7fe6a279da95")
inua_app = knackpy.App(app_id="6088367fcd6220001bef9a90",  api_key="6869438b-5ad5-45ad-add2-7fe6a279da95")

@csrf_exempt
def index(request):
    if request.method == 'POST':
        session_id = request.POST.get('sessionId')
        service_code = request.POST.get('serviceCode')
        phone_number = request.POST.get('phoneNumber')
        if phone_number[0:1] == "+":
            phone_number = phone_number[1:]
        text = request.POST.get('text')
        data = []
        info = text.split('*')

        response = ""
        products = {1:"Cattle ", 2:"Chickens", 3:"Fish", 4:"Horses", 5:"Pigs"}
        categories = {1:"Feeds", 2:"Fertilizer", 3:"Insurance", 4:"Medicine", 5:"Seeds", 6:"Solar"}

        if text == "":
            response = "CON Welcome to Mazao Bora \n"
            # response .= "1. My Account \n"
            response += "1. English\n" \
                        "2. Kiswahili"

        elif text == "1":
            response = "CON Please Select:\n"
            response += "1. Register\n" \
                        "2. Products\n" \
                        "3. Our Agrovets"
        elif text == "2":
            response = "CON Tafadhali Chagua:\n"
            response += "1. Kujiandikisha\n" \
                        "2. Bidhaa Zetu\n" \
                        "3. Agrovets"
        # English Start
        # Registration Start
        elif text == "1*1":
            response = "CON Select category: \n"
            response += "1. Customer\n" \
                        "2. Farmer\n" \
                        "3. Agrovet\n" \
                        "4. Supplier \n " \
                        "5. Logistics"
        # Create Customer

        elif info[1] == '1' and info[2] == '1' and text == "1*1*1":
            response = "CON Enter your first name:"

        elif info[1] == '1' and info[2] == '1' and text == "1*1*"+info[2]+"*"+info[3]:
            response = "CON Enter your last name:"

        elif info[1] == '1' and info[2] == '1' and text == "1*1*"+info[2]+"*"+info[3]+"*"+info[4]:
            response = "CON Enter your Email Address:"

        elif info[1] == '1' and info[2] == '1' and text == "1*1*"+info[2]+"*"+info[3]+"*"+info[4]+"*"+info[5]:
            response = "CON Enter your National ID number:"

        elif info[1] == '1' and info[2] == '1' and text == "1*1*"+info[2]+"*"+info[3]+"*"+info[4]+"*"+info[5]+"*"+info[6]:
            try:
                create_customer(info[3], info[4], phone_number, info[5], info[6])
                response = "END A confirmation message will be sent you shortly. Thank you for choosing Mazao Bora."

            except Exception as e:
                print(f"Houston, we have a problem: {e}")
                response = "END We are experiencing some technical difficulties. " \
                           "Please try again after a short while. Thank you for choosing Mazao Bora."

        # Create Farmer

        elif info[1] == '1' and info[2] == '2' and text == "1*1*2":
            response = "CON Enter your first name:"

        elif info[1] == '1' and info[2] == '2' and text == "1*1*"+info[2]+"*"+info[3]:
            response = "CON Enter your last name:"

        elif info[1] == '1' and info[2] == '2' and text == "1*1*"+info[2]+"*"+info[3]+"*"+info[4]:
            response = "CON Enter your National ID number:"

        elif info[1] == '1' and info[2] == '2' and text == "1*1*"+info[2]+"*"+info[3]+"*"+info[4]+"*"+info[5]:
            try:

                create_farmer(info[3], info[4], phone_number, info[5])
                response = "END A confirmation message will be sent you shortly. Thank you for choosing Mazao Bora."

            except Exception as e:
                print(f"Houston, we have a problem: {e}")
                response = "END We are experiencing some technical difficulties. " \
                           "Please try again after a short while. Thank you for choosing Mazao Bora."

        # Create Agrovet

        elif info[1] == '1' and info[2] == '3' and text == "1*1*3":
            response = "CON Enter Agrovet Name:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3]:
            response = "CON Enter Agrovet County:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3] + "*" + info[4]:
            response = "CON Enter Agrovet Town:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3] + "*" + info[4] + "*" + info[5]:
            response = "CON Enter Agrovet Location:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3]+ "*" + info[4]\
                + "*" + info[5] + "*" + info[6]:
            response = "CON Enter Agrovet Contact Email Address:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3]+ "*" + info[4]\
                + "*" + info[5] + "*" + info[6] + "*" + info[7]:
            response = "CON Enter Agrovet Contact Phone Number:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3] + "*" + info[4]\
                + "*" + info[5] + "*" + info[6] + "*" + info[7] + "*" + info[8]:
            response = "CON Enter Agrovet Contact Name:"

        elif info[1] == '1' and info[2] == '3' and text == "1*1*" + info[2] + "*" + info[3] + "*" + info[4] + "*" + \
                info[5] + "*" + info[6] + "*" + info[7] + "*" + info[8] + "*" + info[9]:
            try:

                create_agrovet(info[3], info[4], info[5], info[6], info[7], info[8], info[9])
                response = "END A confirmation message will be sent you shortly. Thank you for choosing Mazao Bora."

            except Exception as e:
                print(f"Houston, we have a problem: {e}")
                response = "END We are experiencing some technical difficulties. " \
                           "Please try again after a short while. Thank you for choosing Mazao Bora."

        # Create Supplier

        elif info[1] == '1' and info[2] == '4' and text == "1*1*4":
            response = "CON Enter Supplier Name:"

        elif info[1] == '1' and info[2] == '4' and text == "1*1*"+info[2]+"*"+info[3]:
            response = "CON Enter Supplier Email Address:"

        elif info[1] == '1' and info[2] == '4' and text == "1*1*"+info[2]+"*"+info[3]+"*"+info[4]:
            response = "CON Enter Supplier Phone Number:"

        elif info[1] == '1' and info[2] == '4' and text == "1*1*" + info[2] + "*" + info[3] + "*" + info[4]+"*"+info[5]:
            response = "CON Enter Supplier Contact Name:"

        elif info[1] == '1' and info[2] == '4' and text == "1*1*"+info[2]+"*"+info[3]\
                + "*"+info[4]+"*"+info[5]+"*"+info[6]:
            try:

                create_supplier(info[3], info[4], info[5], info[6])
                response = "END A confirmation message will be sent you shortly. Thank you for choosing Mazao Bora."

            except Exception as e:
                print(f"Houston, we have a problem: {e}")
                response = "END We are experiencing some technical difficulties. " \
                           "Please try again after a short while. Thank you for choosing Mazao Bora."

        # Registration End

        # Product Start
        elif text == "1*2":
            response = "CON Please Select Category:\n"
            for key, value in categories.items():
                response += str(key)+". " + value+"\n"

        elif info[1] == '2' and info[2] == '1' and text == '1*2*' + info[2]:
            response = "CON Please Select Category:\n"
            for key, value in products.items():
                response += str(key) + ". " + value + "\n"

        elif info[1] == '2' and text == '1*2*'+info[2]+"*"+info[3]:
            try:
                filters = {"match": "or",
                           "rules": [
                               {"field": "field_676", "operator": "is", "value": products[int(info[3])]},
                           ],
                           }
                records = app.get("object_19", filters=filters, generate=True)
                response = "CON Select Product Code:\n"
                for record in records:
                    response += str(record["field_211"]) + ". " + record["field_301"] + "\n"

            except Exception as e:
                print('Encountered an error while sending: %s' % str(e))
                response = "END We are restocking. One of our agents will reach out to you with more" \
                           " information on this product. " \
                           "Thank you for choosing Mazao Bora"

        elif info[1] == '2' and text == '1*2*'+info[2]+'*'+info[3]+"*"+info[4]:
            response = "CON Enter Product Quantity:\n"

        elif info[1] == '2' and text == '1*2*' + info[2] + '*' + info[3] + '*' + info[4] + "*" + info[5]:
            response = "CON Enter your location:\n"

        elif info[1] == '2' and text == '1*2*' + info[2] + '*' + info[3] + '*' + info[4] + "*" + info[5] \
                + "*" + info[6]:
            try:
                filters = {"match": "or",
                           "rules":
                               [
                                   {"field": "field_211", "operator": "is", "value": info[4]},
                               ],
                           }
                product_records = inua_app.get("object_19", filters=filters, generate=True)
                response = "CON Please Confirm your order:\n"
                for p in product_records:
                    response += "Product: "+p["field_301"]+"\n" \
                                "Quantity:"+info[5]+"\n" \
                                "Location: "+info[6]+"\n" \
                                "1. Yes \n " \
                                "2. No"
            except Exception as e:
                print('Encountered an error while sending: %s' % str(e))
                response = "END We are restocking. One of our agents will reach out to you with more" \
                           " information on this product. " \
                           "Thank you for choosing Mazao Bora"

        elif info[1] == '2' and info[7] == '1' and text == '1*2*' + info[2] + '*' + info[3] + '*' + info[4] + "*" + info[5] \
                + "*" + info[6] + "*" + info[7]:
            return

        elif info[1] == '2' and info[7] == '1' and text == '1*2*' + info[2] + '*' + info[3] + '*' \
            + info[4] + "*" + info[5] + "*" + info[6] + "*" + info[7]:
            return

        # Product End

        # karimi Agrovet Start
        elif text == "1*3":
            if CustomUser.objects.filter(phone_number=phone_number).exists():
                response = "CON 1. Karimi Agrovet Kitale\n " \
                           "2. Karimi Agrovet Kibomet"
            else:
                response = "END You have not registered for this service yet. Kindly contact us on 0113 041 748 " \
                           "for assistance"
        # Karimi Agrovet End

        # English End.
        # Swahili Start

        # S Registration Start
        elif text == "2*1":
            if CustomUser.objects.filter(phone_number=phone_number).exists():
                response = "END Tayari umeshaisajili huduma hii"
            else:
                response = "CON Weka jina lako la kwanza:"

        elif text == "2*1*"+info[2] and info[1] == '1':
            response = "CON Weka jina lako la mwisho:"

        elif text == "2*1*"+info[2]+"*"+info[3] and info[1] == '1':
            response = "CON Jaza nambari yako ya kitambulisho:"

        elif text == "2*1*"+info[2]+"*"+info[3]+"*"+info[4] and info[1] == '1':
            try:
                user = CustomUser(first_name=info[2], last_name=info[3], phone_number=phone_number,
                                  id_number=info[4])
                user.set_password(text.split('*')[4])
                user.save()

            except Exception as e:
                print(f"Houston, we have a problem: {e}")
            response = "END Subiri ujumbe thabiti hivi punde. Asante kwa kuchagua huduma za Mazao Bora."

        # S Registration End

        # S Product Start
        elif text == "2*2":
            categories = Category.objects.all().order_by("pk")
            if Category.objects.exists():
                response = "CON Select product category:\n"
                for cat in categories:
                    response += str(cat.pk)+"."+" "+cat.name+"\n"
            else:
                response = "END We are updating our inventory, we will be in touch with you."

        elif info[1] == '2' and text == '2*2*'+info[2]:
            category = get_object_or_404(Category, pk=info[2])
            products = Product.objects.filter(category=category).order_by("pk")
            if products.exists():
                response = "CON "+category.name+"\n"
                response += "Please select a product \n"
                for prod in products:
                    response += str(prod.pk)+"."+" "+prod.name+"\n"

        elif info[1] == '2' and text == text == '2*2*'+info[2]+'*'+info[3]:
            response = "END One of our agents will reach out to you on the availability of this product. Thank you."

        # S Product End

        # S Training Start
        elif text == "2*3":
            if CustomUser.objects.filter(phone_number=phone_number).exists():
                response = "CON Please enter amount:"
            else:
                response = "END You have not registered for this service yet. Kindly contact us on 0113 041 748 " \
                           "for assistance"
        # S Training End

        # S Payment Start
        elif text == "2*4":
            if CustomUser.objects.filter(phone_number=phone_number).exists():
                response = "CON Please Select:\n"
                response += "1. Last Expense\n" \
                            "2. NHIF\n" \
                            "3. Agrovet Products\n" \
                            "4. Solar"
            else:
                response = "END You have not registered for this service yet. Kindly contact us on 0113 041 748 " \
                           "for assistance"

        # S Payment End
        # Swahili End

        else:
            data.clear()
            response = "END Invalid input. Please try again."

        return HttpResponse(response)


def user_list(request):
    template_name = 'users/list.html'
    all_users = CustomUser.objects.all().order_by('-created_at')

    data = {
        'all_users': all_users,
    }

    return render(request, template_name, data)


def export_user_csv(request):
    farmer_list = CustomUser.objects.all()
    response = HttpResponse('text/csv')
    response['Content-Disposition'] = 'attachment; filename=farmers.csv'
    writer = csv.writer(response)
    writer.writerow(['First Name', 'Last Name', 'ID Number'])
    farmers = farmer_list.values_list('first_name', 'last_name', 'id_number')
    for farmer in farmers:
        writer.writerow(farmer)
    return response


def user_detail(request, pk):
    template_name = "users/user_detail.html"
    user = get_object_or_404(CustomUser, pk=pk)
    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            user_info = form.save(commit=False)
            id_number = form.cleaned_data.get('id_number')
            phone = form.cleaned_data.get('phone_number')
            if phone[0:1] == '0':
                phone = '254%s' % phone[1:]
            elif phone[0:1] == '7':
                phone = '254%s' % phone[0:]
            elif phone[0:1] == '+':
                phone = phone[1:]
            user.password = id_number
            user.set_password(id_number)
            user.phone_number = phone
            user_info.save()

            return redirect('user_detail', pk=pk)

    else:
        form = UserForm(request.POST, instance=user)

    data = {
        'form': form,
        'user': user,
    }

    return render(request, template_name, data)


def create_user(request):
    template_name = 'users/create_user.html'
    if request.method == 'POST':
        form = UserForm(request.POST or None)
        if form.is_valid():
            user = form.save(commit=False)
            id_number = form.cleaned_data.get('id_number')
            phone = form.cleaned_data.get('phone_number')
            if phone[0:1] == '0':
                phone = '254%s' % phone[1:]
            elif phone[0:1] == '7':
                phone = '254%s' % phone[0:]
            elif phone[0:1] == '+':
                phone = phone[1:]
            user.password = id_number
            user.set_password(id_number)
            user.phone_number = phone
            user.save()

            return redirect('all_users')

    else:
        form = UserForm()

    data = {
        'form': form,
    }

    return render(request, template_name, data)


def user_bulk_upload(request):
    template_name = 'users/bulk_upload.html'
    if request.method == 'POST':
        form = BulkUploadForm(request.POST, request.FILES or None)

        if form.is_valid():
            user_csv = form.cleaned_data['user_csv']

            decoded_file = user_csv.read().decode("utf-8")
            io_string = io.StringIO(decoded_file)
            csv_reader = csv.reader(io_string, delimiter=',')
            for account in csv_reader:
                name = account[0].split(' ')
                phone = account[1]
                id_number = account[2]
                first_name = name[0]
                last_name = name[1]
                if phone[0:1] == '0':
                    phone = '254%s' % phone[1:]
                elif phone[0:1] == '7':
                    phone = '254%s' % phone[0:]
                elif phone[0:1] == '+':
                    phone = phone[1:]
                if CustomUser.objects.filter(phone_number=phone) or CustomUser.objects.filter(id_number=id_number):
                    continue
                else:
                    user = CustomUser(first_name=first_name, last_name=last_name, phone_number=phone, id_number=id_number)
                    user.set_password(id_number)
                    user.save()

        return redirect('all_users')
    else:
        form = BulkUploadForm()

    data = {
        'form': form
    }

    return render(request, template_name, data)


class UserAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = CustomUser.objects.all()

        if self.q:
            qs = qs.filter(first_name__istartswith=self.q)

        return qs


def user_groups(request):
    template_name = "users/groups.html"
    groups = UserGroup.objects.all().order_by("-created_at")
    if request.method == 'POST':
        form = GroupForm(request.POST or None)

        if form.is_valid():
            u_group = form.save(commit=False)
            u_group.save()

            return redirect('user_groups')

    else:
        form = GroupForm()

    data = {
        'form': form,
        'user_groups': groups,
    }

    return render(request, template_name, data)


def update_profiles(request):
    users = CustomUser.objects.all()
    for user in users:
        profile, created = UserProfile.objects.get_or_create(user=user)
        profile.save()

    return redirect('all_users')










