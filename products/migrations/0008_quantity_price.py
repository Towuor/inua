# Generated by Django 3.1.2 on 2021-03-19 11:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_quantity_updated_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='quantity',
            name='price',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]
