from django.urls import path
from . import views

urlpatterns = [
    path('', views.category_home, name='categories'),
    path('category/<int:pk>', views.prod_list, name='prod_list'),
    path('product/<int:pk>', views.prod_detail, name='prod_detail'),
    path('sales', views.sale_form, name='sales'),
    path('all-sales', views.sales_home, name='all_sales'),
    path('ajax/load-products', views.load_products, name='ajax_load_products'),
]