from django import forms
from .models import Category, Product, Sale, Quantity


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        exclude = ('created_at',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ('category', 'created_at',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
            'total_quantity': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
        }


class QuantityForm(forms.ModelForm):
    class Meta:
        model = Quantity
        exclude = ('product', 'created_at')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
            'quantity': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
            'price': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
        }


class SaleForm(forms.ModelForm):

    class Meta:
        model = Sale
        fields = ('category', 'product', 'quantity', 'amount',)
        widgets = {
            'category': forms.Select(attrs={'class': 'form-control'}),
            'product': forms.Select(attrs={'class': 'form-control'}),
            'quantity': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
            'amount': forms.TextInput(attrs={'class': 'au-input au-input--full', 'required': 'true'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['product'].queryset = Product.objects.none()

        if 'category' in self.data:
            try:
                category_id = int(self.data.get('category'))
                self.fields['product'].queryset = Product.objects.filter(category_id=category_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['product'].queryset = self.instance.country.city_set.order_by('name')
