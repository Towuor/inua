from django.db import models
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, null=False, blank=False)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='p_category', null=False, blank=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=1000, blank=False, null=False)
    total_quantity = models.CharField(max_length=1000, blank=False, null=False)
    current_quantity = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, null=False, blank=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Quantity(models.Model):
    product = models.ForeignKey(Product, related_name='p_quantity', null=False, blank=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=10, null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True, blank=True)
    price = models.CharField(max_length=1000, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, null=False, blank=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.product.name + " " + self.quantity


class Sale(models.Model):
    category = models.ForeignKey(Category, related_name="sale_category", null=True, blank=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, related_name="sale_product", null=False, blank=False, on_delete=models.CASCADE)
    quantity = models.CharField(max_length=1000, blank=False, null=False)
    amount = models.CharField(max_length=1000, blank=False, null=False)
    created_at = models.DateTimeField(default=timezone.now, null=True, blank=True)

    def __str__(self):
        return self.product.name
