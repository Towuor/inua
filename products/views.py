from django.shortcuts import render, redirect, get_object_or_404
from .models import Category, Product, Sale, Quantity
from .forms import CategoryForm, ProductForm, SaleForm, QuantityForm
from django.contrib import messages


def category_home(request):
    template_name = 'products/category.html'
    categories = Category.objects.all()
    if request.method == 'POST':
        form = CategoryForm(request.POST or None)
        if form.is_valid():
            cat = form.save(commit=False)
            cat.save()

            return redirect('categories')

    else:
        form = CategoryForm()

    data = {
        'categories': categories,
        'form': form
    }

    return render(request, template_name, data)


def prod_list(request, pk):
    template_name = "products/products.html"
    category = Category.objects.get(pk=pk)
    products = Product.objects.filter(category=category)
    if request.method == 'POST':
        form = ProductForm(request.POST or None)
        if form.is_valid():
            t_q = form.cleaned_data.get('total_quantity')
            product = form.save(commit=False)
            product.category = category
            product.current_quantity = t_q
            product.save()

            return redirect()
    else:
        form = ProductForm()

    data = {
        'form': form,
        'products': products,
    }

    return render(request, template_name, data)


def prod_detail(request, pk):
    template_name = "products/product.html"
    prod = get_object_or_404(Product, pk=pk)
    quants = Quantity.objects.filter(product=prod)
    if request.method == 'POST':
        form = QuantityForm(request.POST or None)
        if form.is_valid():
            quantity = form.save(commit=False)
            quantity.product = prod
            quantity.save()

            return redirect('prod_detail', pk=pk)
    else:
        form = QuantityForm()

    data = {
        'form': form,
        'quants': quants,
        'prod': prod
    }

    return render(request, template_name, data)


def sale_form(request):
    template_name = "products/sales.html"
    if request.method == 'POST':
        form = SaleForm(request.POST or None)
        if form.is_valid():
            quantity = form.cleaned_data.get('quantity')
            sale_product = form.cleaned_data.get('product')
            prod = Product.objects.get(name=sale_product)
            c_q = prod.current_quantity
            n_q = float(c_q) - float(quantity)
            sale = form.save(commit=False)
            prod.current_quantity = n_q
            prod.save()
            sale.save()
            messages.info(request, "Transaction successful. Thank You.")
            return redirect('sales')
    else:
        form = SaleForm()
    data = {
        'form': form,
    }

    return render(request, template_name, data)


def sales_home(request):
    template_name = "products/all_sales.html"
    sales = Sale.objects.all().order_by("-created_at")

    data = {
        'sales': sales,
    }

    return render(request, template_name, data)


def load_products(request):
    category_id = request.GET.get('category')
    products = Product.objects.filter(category__id=category_id)
    return render(request, 'products/products_list.html', {'products': products})
