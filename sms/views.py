from django.shortcuts import render, redirect, get_object_or_404
from .models import SMS
from .forms import SMSForm
import africastalking
from users.models import CustomUser
# Create your views here.

username = "inua_mkulima"
api_key = "f398a3e6c44e3fd30837a62ba12ee1c534ec703ba336541ebd55f4b3fc937dee"
africastalking.initialize(username, api_key)
sms = africastalking.SMS
sender_id = 'KARIMI-LTD'


def sms_home(request):
    template_name = 'messages/messages.html'
    sms_messages = SMS.objects.all().order_by("-created_at")
    if request.method == 'POST':
        form = SMSForm(request.POST or None)
        if form.is_valid():
            sms_message = form.save(commit=False)
            content = form.cleaned_data['message_content']
            group = form.cleaned_data['group']
            phone_numbers = []
            all_users = CustomUser.objects.all()
            le_users = CustomUser.objects.filter(profile__last_expense=True)
            nhif_users = CustomUser.objects.filter(profile__nhif=True)

            print(le_users.count)
            print(nhif_users.count)

            for user in all_users:
                phone_number = "+"+user.phone_number
                phone_numbers.append(phone_number)

            recipients = phone_numbers

            # Set your message
            message = content

            # Set your shortCode or senderId
            sender = sender_id
            try:
                # Thats it, hit send and we'll take care of the rest.
                response = sms.send(message, recipients, sender)
                print(response)
            except Exception as e:
                print('Encountered an error while sending: %s' % str(e))

            sms_message.save()

    else:
        form = SMSForm()

    data = {
        'form': form,
        'sms': sms_messages,
    }

    return render(request, template_name, data)
