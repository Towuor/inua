from django.db import models
from django.utils import timezone

# Create your models here.


class SMS(models.Model):
    GROUP = [
        ('Everyone', 'Everyone'),
        ('Last Expense', 'Last Expense'),
        ('NHIF', 'NHIF'),
    ]
    message_content = models.TextField(null=False, blank=False)
    group = models.CharField(max_length=15, blank=True, null=True, choices=GROUP, default=None)
    message_success = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now, blank=False, null=False)

    def __str__(self):
        return self.message_content
