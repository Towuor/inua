from django import forms
from .models import SMS


class SMSForm(forms.ModelForm):
    class Meta:
        model = SMS
        exclude = ('created_at', )
        widgets = {
            'message_content': forms.Textarea(attrs={'class': 'form-control', 'required': 'true'}),
            'group': forms.Select(attrs={'class': 'form-control', 'id': 'select',})
        }